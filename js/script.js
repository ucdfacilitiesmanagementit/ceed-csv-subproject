var requestData = function(path, type) {
  var request = new XMLHttpRequest();
  request.open('GET', path, true);
  request.onload = function() {
    if (this.status >= 200 && this.status < 400) {   // Success!

      var data = JSON.parse(this.response);
      if (type === 'demand') {
        buildDemandChart(data);
      }else if (type === 'usage') {
        buildUsageChart(data);
      }else {
        console.log('Error, invalid data type');
      }

    } else {
      console.log('Server connected successfully but returned an error');
    }
  };
  request.onerror = function() {
    console.log('Server did not connect successfully');
  };
  request.send();
};

var buildDemandChart = function(demandData) {
  var demandChart = new Highcharts.Chart({
    chart: {
      type: 'area',
      renderTo: 'demand-container',
      events: {
          load: function() {
            //export csv file
            var chart = this;
            if (document.getElementById('csvButton-demand')) {
              document.getElementById('csvButton-demand').addEventListener('click', function() {
                var csv = chart.getCSV();
                csv = csv.replace('ELECTRICITY', 'ELECTRICITY (kBTU/hour)');
                csv = csv.replace('CHILLED WATER', 'CHILLED WATER (kBTU/hour)');
                csv = csv.replace('STEAM', 'STEAM (kBTU/hour)');
                csv = csv.replace('NATURAL GAS', 'NATURAL GAS (kBTU/hour)');
                var blob = new Blob([csv], {
                  'type': 'text/csv;charset=utf8;'
                });
                var link = document.createElement('a');
                var filename = 'CEED_ghausi_current_demand.csv';

                if (link.download !== undefined) { // feature detection
                  // Browsers that support HTML5 download attribute
                  link.setAttribute('href', window.URL.createObjectURL(blob));
                  link.setAttribute('download', filename);
                  document.body.appendChild(link);
                  link.click();
                  document.body.removeChild(link);
                  window.URL.revokeObjectURL(blob);
                } else {
                  alert('CSV export currently only works with recent versions of Chrome, Firefox, and Opera.\n\n' +
                    'Please try again in one of these browsers.\n\n' +
                    'We apologize for the inconvenience.');
                }
              });
            }
          }
        }
    },
    title: {
      text: 'Ghausi Demand'
    },
    xAxis: {
      type: 'datetime',
    },
    yAxis: {
      title: {
          text: 'kBTU'
      }
    },
    series: [{
        name: 'CHILLED WATER',
        data: demandData.chilledWater
      }, {
        name: 'STEAM',
        data: demandData.steam
      }, {
        name: 'NATURAL GAS',
        data: demandData.gas
      }, {
        name: 'ELECTRICITY',
        data: demandData.electricity
    }]
  });
};

var buildUsageChart = function(usageData) {
  var usageChart = new Highcharts.Chart({
    chart: {
      type: 'column',
      renderTo: 'usage-container',
      events: {
        load: function() {
          //export csv file
          var chart = this;
          if (document.getElementById('csvButton-usage')) {
            document.getElementById('csvButton-usage').addEventListener('click', function() {
              var csv = chart.getCSV();
              csv = csv.replace('ELECTRICITY', 'ELECTRICITY (kBTU/hour)');
              csv = csv.replace('CHILLED WATER', 'CHILLED WATER (kBTU/hour)');
              csv = csv.replace('STEAM', 'STEAM (kBTU/hour)');
              csv = csv.replace('NATURAL GAS', 'NATURAL GAS (kBTU/hour)');
              var blob = new Blob([csv], {
                'type': 'text/csv;charset=utf8;'
              });
              var link = document.createElement('a');
              var filename = 'CEED_ghausi_current_usage.csv';

              if (link.download !== undefined) { // feature detection
                // Browsers that support HTML5 download attribute
                link.setAttribute('href', window.URL.createObjectURL(blob));
                link.setAttribute('download', filename);
                document.body.appendChild(link);
                link.click();
                document.body.removeChild(link);
                window.URL.revokeObjectURL(blob);
              } else {
                alert('CSV export currently only works with recent versions of Chrome, Firefox, and Opera.\n\n' +
                  'Please try again in one of these browsers.\n\n' +
                  'We apologize for the inconvenience.');
              }
            });
          }
        }
      }
    },
    title: {
      text: 'Ghausi Usage'
    },
    yAxis: {
      title: {
        text: 'kBTU'
      }
    },
    series: [{
        name: 'CHILLED WATER',
        data: usageData.chilledWater
      }, {
        name: 'STEAM',
        data: usageData.steam
      }, {
        name: 'NATURAL GAS',
        data: usageData.gas
      }, {
        name: 'ELECTRICITY',
        data: usageData.electricity
    }]
  });
};

var demandPath = '/data/CEED_ghausi_demand.json';
var usagePath = '/data/CEED_ghausi_usage.json';

requestData(demandPath, 'demand');
requestData(usagePath, 'usage');
