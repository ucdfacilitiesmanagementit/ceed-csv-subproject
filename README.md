# README #

### What is this repository for? ###

This is a subproject to work on enhancing the functionality of the CSV download buttons in CEED. It contains the Highstock library, the export-csv plugin for Highstock/Highcharts, and sample usage/demand energy datasets for Ghausi Hall.

### How do I get set up? ###

* Run `git clone git@bitbucket.org:ucdfacilitiesmanagementit/ceed-csv-subproject.git`
* Start a simple web server in your project folder, e.g.
    - `$ php -S 127.0.0.1:8080` or
    - `$ python -m SimpleHTTPServer 8080`
* Navigate to `localhost:8080` in your web browser to begin testing the app

### Who do I talk to? ###

Matt Sidor (msidor@ucdavis.edu)